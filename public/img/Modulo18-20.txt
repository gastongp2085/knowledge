Modulo 18: IOT HACKING

Internet de las Cosas (IOT)
OWASP TOP 10 - IOT Vulnerabilities

DOS
RCE
Ejecutar Exploits
********************************************
Módulo 19: Cloud Computing

Conceptos:
SASS (software as a service)
PASS (platform as a service)
IASS (Infraestructure as a service)

Tipos de nube:
- PUBLICA -->
- PRIVADA --> EXCLUSIVA PARA UN CLIENTE
- HYBRID  --> AWS

AWS | AZURE | ADOBE | ORACLE | GOOGLE

Serverless:
BD
AD
FILESERVER
APP

EC2 --> Servidores
RDS --> Base de Datos
S3 --> almacenamiento

A los procesos de ETHICAL HACKING en la nube
1. aws | azure se le tiene que solicitar permiso para un EH

Nuevos riesgos:
1. Confidencialidad: cifrado de disco duro , base de datos

*********************************************
Módulo 20: Criptografía

Tipos:

A. SIMÉTRICO	: utiliza una llave para encriptar y la misma llave para desencriptar
B. ASIMÉTRICO	: llaves públicas y privadas, llaves diferentes para encriptar y desencriptar

Existe una relacion MATEMATICA entre la llave pública y privada

llave pública --> (no puedo) LLAVE PRIVADA

CIPHERS: Algoritmos usados para encriptar y desencriptar

Ciphers simétricos
RC4
AES
DES
3DES

Ciphers asimétricos
RSA
DIFFIE HELLMAN

ciphers HASH
MD4
MD5
SHA
SHA1

PKI (Public Key Infraestructure)

-- YA NO SE USAN-- 
SSLv2, SSLv3, TLS1,  TLS1.1, TLS1.2, TLS1.3(*)
SSLv3: POODLE, SWEET32, HEARTBLEED

Certificados digitales
CA: Autoridad certificado que emite certificados, los revoca, brinda propiedades
CA RAIZ: no esta pública en Internet
CA INTERMEDIARIAS: COMODO, GODADDY

Self-signed certificate: Certificados autofirmados
CRL: Certificate Revocation List
OCSP: Online Certificate Status Protocol Responder














