#! /bin/bash
#Le pasamos un parametro para describir el nombre del commit
#el parametro no debe ir entre comillas, y sin espacios en blanco

if [ "$1" = "" ]; then
    # $var is empty
    echo "Debe proporcionar nombre del commit"
    echo "No se realizará ningún cambio"
    echo "Proceso terminado..."
else
    git add .
    git commit -m $1
    git push -u origin master
    git push -u origin --all
    git push -u origin --tags
fi