Módulo 10: Denegación de Servicio (Denied of Service - DOS)
----------------------------------------------------------------------

Concepto: Detener un servicio legitimo a traves de saturacion
DDOS (DOS Distribuido): BOTNETS para generar DDOS

Tipos:
A. Ataques volumétricos: consumir el ancho de banda
PING OF DEATH: Paquete excedia el tamaño de las especificaciones TCP/IP
ICMP FLOOD ATTACK

B. Ataques sobre protocolos
HTTP
SYN FLOOD
ACK FLOOD

C. Ataques a nivel de Capa de Aplicación
Consumir los recursos de la aplicación
POST | GET 
Saturar procesador, saturar memoria RAM, saturar escritura en disco

¿COMO DETENGO?

1. ancho de banda, performance, balanceadores, alta disponibilidad
2. ARQUITECTURA DISTRIBUIDAS
3. OSIPTEL
4. SOLUCIONES ANTI DDOS (EN LA NUBE)

IMPERVA
CLOUDFLARE

			      (SERVICIO)			IP PUBLICA
			      CLOUDFLARE			SERVIDOR
-----				----				-------
			----->	10K		----->
-----				----				-------	
USUARIO				BOTs				www.elpalomo.pe
www.elpalomo.pe			DDOS


IP ---------------------------------------------------------->






